#!/usr/bin/python
#Dar formato a un string f ’texto’ con .format()


if __name__ == "__main__":
	nombre="José Pérez"
	string_formato=f"La persona se llama {nombre}"
	print("sin nombre (string de formato)", string_formato)
	string_formateado=string_formato.format()
	print("Con nombre: ")
	print(string_formateado)
