#!/usr/bin/python

if __name__ == "__main__":
	frase="%s." %input("Hola, ¿Me dirias, por favor, tu frase favorita?\n")
	print("Tu frase favorita es: %s" %frase)
	
	#funcion len para conocer el largo de la frase
	largo=len(frase)
	print("El largo de la frase es %d" %largo)
	
	#funcion split para convertir una cadena en una lista, utilizando un eparador
	lista_palabras=frase.split()
	print(lista_palabras)
	print("La cantidad de palabras es: %s" %len(lista_palabras))
	
	#Entregando partes de la frase con un separador ","
	partes=frase.split(",")
	print(partes)
	print("La cantidad de partes es: %s" %len(partes))

	#funcion .index() Entregar la posicion de la busqueda en la frase 
	buscar=input("Qué deseas buscar?")
	posicion=frase.index(buscar)
	print("La posición de lo que buscas está en el caracter %d" %posicion)
	print("Dice desde tu busqueda: %s" %frase[posicion::])
	print("En mayúsculas: ", frase.upper())
	print("En minusculas: ", frase.lower())
	print("*funcion capitalice* Primera letra mayúscula: ", frase.capitalize())
	
#Recortes y posiciones
	#Recortar un string
	recorte=frase
	print(recorte[0:2])
