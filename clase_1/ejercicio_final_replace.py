#!/usr/bin/python
if __name__ == "__main__":
	texto=input("Por favor, introduce un texto: ")
	print(" + Programa dice:")
	print(" - Tu texto es: %s" %texto)
	
	largo_texto=len(texto)
	if largo_texto>=15:
		print("El texto tiene más de 15 caracteres")
	
	#Cantidad de caracteres
	largo=len(texto)
	print("El largo del texto *contando los espacios* es: %d" %largo)
	
	#formateado a minusculas
	print(texto[0:15].lower())
	
	#funcion replace -> reemplazando espacio " " por "_"
	print(texto.replace(" ", "_"))
	
	print(texto[0:15])
