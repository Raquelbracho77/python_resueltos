#!/usr/bin/python

if __name__ == "__main__":
	string_formato="La persona se llama %s su edad es %d"
	print("Sin nombre (string de formato)", string_formato)
	nombre="Jośe Pérez"
	edad=40
	string_formateado=f"La persona se llama {nombre} y su edad son {edad} años"
	print("Con nombre y edad: ")
	print(string_formateado)
