from nombres import p1,p2,p3
from edades import ep1, ep2, ep3

if __name__ == "__main__":
	print(p1,p2,p3)
	print(ep1,ep2,ep3)
	puntaje = 0
	aciertos = []
	
	# es un string
	edad_p1 = input(f"Cual es la edad de {p1}")
	print(f"Tu pusiste para {p1} la edad de {edad_p1}")
	
	if int(edad_p1)==ep1:
		print("Acertaste")
		puntaje += 1
		aciertos.append(p1)
	else:
		print(f"Fallaste en adivinar la edad de {p1}")
		
	edad_p2 = input(f"Cual es la edad de {p2}")
	print(f"Tu pusiste para {p2} la edad de {edad_p2}")
	
	if int(edad_p2)==ep2:
		print("Acertaste")
		puntaje += 1
		aciertos.append(p2)
	else:
		print(f"Fallaste en adivinar la edad de {p2}")		
	
	edad_p3 = input(f"Cual es la edad de {p3}")
	print(f"Tu pusiste para {p3} la edad de {edad_p3}")
	
	if int(edad_p3)==ep3:
		print("Acertaste")
		puntaje += 1
		aciertos.append(p3)
	else:
		print(f"Fallaste en adivinar la edad de {p3}")		
	#==========================
	
	print("Tu puntaje final es %d" %puntaje)
	print("Tus aciertos fueron para %s" %", ".join(aciertos))
