persona = ("Raquel","Bracho",25)
alimentacion = "omnı́vora"
# alimentacion a tupla:
perfil = persona + (alimentacion,)
print("Modo 1", perfil)
# con expansión o *unpack*
perfil = *persona, alimentacion
print("Modo 2", perfil)
