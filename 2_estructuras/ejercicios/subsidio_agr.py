from itertools import permutations
from itertools import groupby
import math

if __name__ == "__main__":
    agricultores=[
      ("Martinez",8 ),
      ("Soto",6.5 ),
      ("Osorio",9.3),
      ("Pineda",33),
      ("Jara",42),
      ("Hernandez",5),
      ("Acuña",12),
      ("Oña",54),
      ("Perez",3),
      ("Nuñez",60)
    ]
    
    
    for agricultor, tasa in agricultores:
        print(f"Para {agricultor} la tasa es {tasa}")        
        
    tasa_limite = 15
    nombres = []
    subsidios = []
    for agricultor, tasa in agricultores:
        print(f"Para {agricultor} la tasa es {tasa}")
        if tasa<=tasa_limite:
            nombres.append(agricultor)
            print(f"Agricultor {agricultor} recibe el subsidio")
            subsidio = math.ceil(-(70/15)*tasa+90)
            print(f"Recibirá un subsidio del {subsidio} %")
            subsidios.append(subsidio)
            
    agricultores_seleccion = list(zip(nombres, subsidios))
            
    """
    Dificil:
    encontrar las permutaciones que si tendrán un mejor subsidio al asociarse
    """
    print(len(nombres), len(subsidios))
    [print(n,t) for n,t in zip(nombres,subsidios)]
    # obtener permutaciones de seleccion con subsidio
    permutaciones = list(permutations(nombres, 3))

    # se define funcion para agrupar según líder
    def getnombre(tupla):
        return tupla[0]
        
    def getsubsidio(tupla):
        return tupla[1]
        
        
    # se inicializan las listas
    grupos=[]
    llaves_grupos=[]
    
    # se agrupa por agricultor lider
    for agr_name, group_iter in groupby(permutaciones, key=getnombre):
        grupos.append(list(group_iter))
        llaves_grupos.append(agr_name)
            
    print("Agrupaciones")
    [print(n,g) for n,g in zip(llaves_grupos,grupos) ]
    
    def subsidio(nombre, agricultores_seleccion):
        for agr, tasa in agricultores_seleccion:
            if nombre==agr:
                return agr, tasa
        
    def subsidio_grupo(grupo, agricultores_seleccion):
        subsidio_group = [subsidio(nombre, agricultores_seleccion) for nombre in grupo]
        return grupo, subsidio_group[0][1]+sum([agr_sbs[1] for agr_sbs in subsidio_group[1:]])*.5
        
    def mejores_select(subsidios, n=5):
        sbs_ordenadas=sorted(subsidios, key=getsubsidio, reverse=True)
        count = 0
        past_sbs = 0 
        tos = []
        for group, sbs in sbs_ordenadas:
            if (sbs > past_sbs or sbs < past_sbs) and count>n:
                break
            elif sbs==past_sbs or count>0:
                tos.append((group, sbs))
            past_sbs = sbs            
            count += 1            
        return tos
        
    subsidios_agrupados = []
                
    for pos, nombre in enumerate(llaves_grupos):
        subsidios_agrupados.append(
            mejores_select(
                [subsidio_grupo(grupo, agricultores_seleccion) for grupo in grupos[pos]], n=5
                )
            )

    print(len(nombres), len(subsidios))
    [print(n,t) for n,t in zip(nombres,subsidios)]
            
    # Resultadoas
    
    for pos, nombre in enumerate(llaves_grupos):
        print("Agricultor ", nombre)
        for group, val in subsidios_agrupados[pos]:
            print("Tasa subsidio:", val)
            print("Grupo: ", group)
