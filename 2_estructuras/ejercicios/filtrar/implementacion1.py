import math
if __name__ == "__main__":
	agricultores=[
		("Martinez",8 ),
		("Soto",6.5 ),
		("Osorio",9.3),
		("Pineda",33),
		("Jara",42),
		("Hernandez",5),
		("Acuña",12),
		("Oña",54),
		("Perez",3),
		("Nuñez",60)
	]
	tasa_limite = 15
	for agricultor, tasa in agricultores:
		print(f"Para {agricultor} la tasa es {tasa}")
		if tasa<=tasa_limite:
			print(f"Agricultor {agricultor} recibe el subsidio")
			subsidio = math.ceil(-(70/15)*tasa+90)
			print(f"Recibirá un subsidio del {subsidio} %")
