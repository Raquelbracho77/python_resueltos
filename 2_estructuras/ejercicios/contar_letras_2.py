if __name__ == "__main__":
	frase = input("Ingresa frase\n").lower()
	lista_frase = list(frase)
	letra = input("Ingresa letra\n").lower()
	filtro=[1 for caracter in lista_frase if caracter==letra]
	contador=sum(filtro)
	print(f"La letra {letra} tiene {contador} ocurrencias en la frase")
