if __name__ == "__main__":
	frase = input("Ingresa frase\n").lower()
	lista_frase = list(frase)
	letra = input("Ingresa letra\n").lower()
	def equals(caracter):
		return caracter==letra
	filtro=list(filter(equals, lista_frase))
	contador=len(filtro)
	print(f"La letra {letra} tiene {contador} ocurrencias en la frase")
