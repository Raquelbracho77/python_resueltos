persona=("Raquel","Bracho",25)
#agregando otra tupla
alimentacion="omnívora"
#alimentación a tupla
perfil=persona + (alimentacion,)
print("Modo 1", perfil)
#con expensión o *unpack*
perfil=*persona, alimentacion
print("Modo 2", perfil)
#fin de la nueva tupla
nombre=persona[0]
apellido=persona[1]
edad=persona[2]
print("La tupla", persona)
print("Datos extraidos:", nombre, apellido, edad)
print("Expansión de tupla:", *persona)
print("Tupla invertida:", persona[::-1])
print("Tupla 1:", persona[0:1])
print("Nombre invertido:", nombre[::-1])

test="Juan"
test in perfil
test="Raquel"
test in perfil
test=25
test in perfil
print("ver %s"%test)


